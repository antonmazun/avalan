from django.shortcuts import render, render_to_response, redirect
from django.conf import settings
from .models import Service, DescriptionAboutUs, CallBackForm, Contact
from django.http import HttpResponse, JsonResponse
from django.template.loader import get_template
from django.template import Context
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives

import requests


# Create your views here.


def index(request):
    ctx = {}
    ctx['LANGUAGES'] = settings.LANGUAGES
    ctx['SELECTEDLANG'] = request.LANGUAGE_CODE
    ctx['services'] = Service.objects.all()
    ctx['descriptions'] = DescriptionAboutUs.objects.all()
    ctx['contacts_phone'] = Contact.objects.filter(type='phone')
    ctx['contacts_email'] = Contact.objects.filter(type='email')
    ctx['contacts_skype'] = Contact.objects.filter(type='skype')
    # ctx['redirect_to'] = request.get_full_path()
    return render(request, 'index.html', ctx)


def call_back(request):
    if request.is_ajax():
        if request.method == 'POST':
            name = request.POST.get('name')
            phone = request.POST.get('phone')
            email = request.POST.get('email')
            text = request.POST.get('text')
            CallBackForm.objects.create(name=name,
                                        phone_number=phone,
                                        email=email,
                                        msg=text
                                        )
            context_email = {
                'name': name,
                'phone': phone,
                'email': email,
                'text': text,
            }
            msg_to_telegramm = 'Ім’я: {}\n Номер телефону: {}\n E-mail: {}\n Текст:{}'.format(name, phone, email, text)
            bot_token = '796870864:AAE0ej6RjZCHEcGxh8XH-WLzPFi7U5j1he0'
            bot_chatID = '-1001288347220'
            send_text = 'https://api.telegram.org/bot' \
                        + bot_token + '/sendMessage?chat_id=' \
                        + bot_chatID + '&parse_mode=Markdown&text=' + msg_to_telegramm
            response = requests.get(send_text)
            print(response.json())
            # send_mail(email=email, reciever=True)
            send_mail(context_email=context_email)
            return JsonResponse({
                'status': True
            })


def send_mail(reciever=False, **kwargs):
    if reciever:
        subject, from_email = 'Нова заявка!', 'kpi.study1@gmail.com'
        message = render_to_string('snippets/user_mail.html', {})
        msg = EmailMultiAlternatives(subject, 'test!', from_email, [kwargs['email']])
    else:
        subject, from_email = 'Заявка для AVALAN.studio!', 'kpi.study1@gmail.com'
        message = render_to_string('snippets/email_template.html', kwargs['context_email'])
        msg = EmailMultiAlternatives(subject, 'test!', from_email, [from_email])
    msg.attach_alternative(message, "text/html")
    msg.send()


def handler404(request, exception, template_name="404.html"):
    print('404!!!!!!!')
    response = render_to_response("404.html")
    response.status_code = 200
    url  = '/' + request.LANGUAGE_CODE + '/'
    return redirect(url)

#
# def handler500(request, exception, template_name="404.html"):
#     return redirect(request.get_absolute_url())
