from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.
from solo.models import SingletonModel
from .svg_model_class import SVGAndImageField


class FirstScreen(SingletonModel):
    content = RichTextUploadingField(verbose_name='Текст')
    btn_text = models.CharField(max_length=255, verbose_name='Кнопка')

    # def __str__(self):
    #     return self.content[:15] + "..."

    class Meta:
        verbose_name = 'Перший екран'
        verbose_name_plural = 'Перший екран'
        # translatable_fields = ("content", "btn_text",)


class Service(models.Model):
    # icon  = models.ImageField(upload_to='service_icons/' , verbose_name='Іконка')
    icon = SVGAndImageField()
    title = models.CharField(max_length=255, verbose_name='Назва')
    image = models.ImageField(upload_to='service_image/', blank=True, null=True, verbose_name='Фото')
    text = RichTextUploadingField(verbose_name='Опис послуги')

    def __str__(self):
        return '{} {}'.format(self.title, self.text[:40] if len(self.text) > 40 else self.text)


class DescriptionAboutUs(models.Model):
    title = models.CharField(verbose_name='Заголовок блоку', max_length=255)
    text = RichTextUploadingField(verbose_name='Опис блоку')

    def __str__(self):
        return self.title


class CallBackForm(models.Model):
    name = models.CharField(max_length=255, verbose_name='Ім’я клієнта')
    phone_number = models.CharField(max_length=255, verbose_name='Номер телефону',
                                    default='', blank=True, null=True)
    email = models.CharField(max_length=255, verbose_name='e-mail',
                             default='', blank=True, null=True)
    msg = models.TextField(max_length=1000, verbose_name='Повідомлення',
                           default='', blank=True, null=True)
    date_send = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False, verbose_name='Відповіли')

    def __str__(self):
        return 'Заявка від {}, дата -  {} ,  статус - {}'.format(self.name, self.date_send,
                                                                 "Відповіли" if self.status else 'Не переглянута')

    class Meta:
        verbose_name = 'Заявка від клієнта'
        verbose_name_plural = 'Заявки від клієнтів'


class Text(SingletonModel):
    text = RichTextUploadingField(verbose_name='Текст про нас')

    def __str__(self):
        return self.text[:25]

    class Meta:
        verbose_name = 'Текст про нас'


class Contact(models.Model):
    TYPE_CONTACT = (
        ('phone', 'Мобільний телефон'),
        ('email', 'Email'),
        ('skype', 'Skype'),
    )

    type = models.CharField(choices=TYPE_CONTACT, max_length=100, verbose_name='Тип контакту')
    contact = models.CharField(verbose_name='Контакт' ,max_length=100)

    def __str__(self):
        return '{} {}'.format(self.get_type_display() , self.contact)
