from modeltranslation.translator import translator, TranslationOptions, register
from .models import FirstScreen, Service, DescriptionAboutUs, Text


@register(FirstScreen)
class FirstScreenTranslationOptions(TranslationOptions):
    fields = ('content', 'btn_text')
    required_languages = ('ru', 'ua', 'en')


@register(Service)
class ServiceTranslationOptions(TranslationOptions):
    fields = ('title', 'text')
    required_languages = ('ru', 'ua', 'en')


@register(DescriptionAboutUs)
class DescriptionAboutUsTranslationOptions(TranslationOptions):
    fields = ('title', 'text')
    required_languages = ('ru', 'ua', 'en')


@register(Text)
class TextTranslationOptions(TranslationOptions):
    fields = ('text',)
    required_languages = ('ru', 'ua', 'en')
