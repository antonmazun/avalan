from django.urls import path, include , re_path
from . import views

urlpatterns = [
    path('', views.index),
    path('callback-form' , views.call_back , name='call-back'),
    re_path(r'^rosetta/', include('rosetta.urls')),
# re_path(r'^rosetta/', include('rosetta.urls')),
]
