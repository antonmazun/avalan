import $ from "jquery";
import AOS from "aos";
import slick from 'slick-carousel';
import Inputmask from "inputmask";
import {getCookie, setCookie} from "./cookies.js";

AOS.init(

);

class Root {
    constructor() {
        this.services_btn = $('.js-change');
        this.form = $('#js-submit-form');
        this.header_height = 80;
    }

    init() {
        this.scroll_header();
        this.change_services(this.services_btn);
        this.slick_slider();
        this.mask_phone();
        this.submit_form(this.form);
        this.anchor_scroll();
    }


    anchor_scroll() {
        let selector  = 'a[href^="#"]';
        $(selector).on('click', function (event) {
            event.preventDefault();
            $(selector).removeClass('active-tab');
            $(this).addClass('active-tab');
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 80
            }, 500);
        });
    }

    submit_form(form) {
        form.on('submit', function (e) {
            e.preventDefault();
            let name = $(this).find('input[name="name"]').val();
            let phone = $(this).find('input[name="phone"]').val();
            let email = $(this).find('input[name="email"]').val();
            let text = $(this).find('input[name="post"]').val();
            let ajax_url = $(this).attr('action');
            let csrf_token = getCookie("csrftoken");
            let form = this;
            $('.js-form').addClass('blured');
            $.ajax({
                method: this.method,
                url: ajax_url,
                data: {
                    name: name,
                    phone: phone,
                    email: email,
                    text: text,
                },
                headers: {
                    "X-CSRFToken": csrf_token
                }
            }).done(function (response) {
                console.log(response);
                console.log(form);
                form.reset();
                $('.js-form').removeClass('blured');
                $('.js-form-wrap').addClass('hidden');
                $('.js-thanks').addClass('visible');
            });
        })
    }

    mask_phone() {
        let selector = document.getElementById("js_mask_phone");
        let im = new Inputmask("+380(99) 999-9999");
        im.mask(selector);
    }

    change_services(btns) {
        btns.on('click', function (e) {
            let _id = $(this).data('id');
            $('.js-title-service').removeClass('active');
            $(this).find('.js-title-service').addClass('active');
            $('.js-service-card').removeClass('visible');
            let selector_active_block = '#' + _id;
            $(selector_active_block).addClass('visible');
        })
    }

    scroll_header() {
        let height_first_screen = $('.first-screen').height();
        $(window).scroll(function (e) {
            if ($(this).scrollTop() >= height_first_screen) {
                $('#headers').addClass('scrolled');
            } else {
                if ($('#headers').hasClass('scrolled') && $(this).scrollTop() === 0) {
                    $('#headers').removeClass('scrolled');
                }
            }
        })
    }

    slick_slider() {
        $('#right_info_slider').slick({
            prevArrow: $('.custom-arrow-prev'),
            nextArrow: $('.custom-arrow-next'),
            infinite: true,
            dots: false,
            autoplay: true,
            speed: 1000,
            autoplaySpeed: 4000,
            appendDots: $('.custom-dots'),
        });
        console.log(slick);
    }
}

var App = new Root();
window.App = App;


window.onload = function () {


    var karl = function () {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            document.getElementById("phone__namber").innerHTML = '<a href="tel:+38 +380960803928 ">+380960803928</a>';
            ``
        } else {
            document.getElementById("phone__namber").innerHTML = '+380960803928 ';
        }
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            document.getElementById("phone1").innerHTML = '<a href="tel: +380960803928 ">+380960803928</a>';
        } else {
            document.getElementById("phone1").innerHTML = '+380960803928 ';
        }
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            document.getElementById("phone2").innerHTML = '<a href="tel:+380635901359 ">+380635901359</a>';
        } else {
            document.getElementById("phone2").innerHTML = '+380635901359 ';
        }
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            document.getElementById("phone3").innerHTML = '<a href="tel: +380 63 785 94 29 ">0637859429</a>';
        } else {
            document.getElementById("phone3").innerHTML = '0637859429 ';
        }
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            document.getElementById("phone4").innerHTML = '<a href="tel: +380 0 97 755 12 14">0977551214</a>';
        } else {
            document.getElementById("phone4").innerHTML = '0977551214 ';
        }
    }
}